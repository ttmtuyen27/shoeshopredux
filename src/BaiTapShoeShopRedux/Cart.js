import React, { Component } from "react";
import { connect } from "react-redux";
import { TANG_GIAM_SO_LUONG, XOA_SAN_PHAM } from "./redux/constants/constants";

class Cart extends Component {
  render() {
    return (
      <div className="container">
        <h4 className="my-5">
          Số lượng sản phẩm trong giỏ hàng: {this.props.gioHang.length}
        </h4>
        {this.props.gioHang.length > 0 && (
          <table className="table">
            <thead>
              <td>ID</td>
              <td>Tên sản phẩm</td>
              <td>Giá sản phẩm</td>
              <td>Số lượng</td>
              <td>Thao tác</td>
            </thead>
            <tbody>
              {this.props.gioHang.map((item) => {
                return (
                  <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleTangGiamSoLuong(item.id, 1);
                        }}
                        className="btn btn-success"
                      >
                        Tăng
                      </button>
                      <span className="mx-3"> {item.soLuong}</span>
                      <button
                        onClick={() => {
                          this.props.handleTangGiamSoLuong(item.id, -1);
                        }}
                        className="btn btn-secondary"
                      >
                        Giảm
                      </button>
                    </td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleXoaSanPham(item.id);
                        }}
                        className="btn btn-danger"
                      >
                        Xóa
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeShopReducer.gioHang,
  };
};

let mapDispatchToProp = (dispatch) => {
  return {
    handleXoaSanPham: (id) => {
      dispatch({
        type: XOA_SAN_PHAM,
        payload: id,
      });
    },
    handleTangGiamSoLuong: (id, giaTri) => {
      dispatch({
        type: TANG_GIAM_SO_LUONG,
        payload: { id: id, giaTri: giaTri },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProp)(Cart);
