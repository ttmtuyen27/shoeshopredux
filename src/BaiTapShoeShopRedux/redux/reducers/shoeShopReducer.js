import { dataShoeShop } from "../../dataShoeShop";
import {
  ADD_TO_CART,
  TANG_GIAM_SO_LUONG,
  XOA_SAN_PHAM,
} from "../constants/constants";

let initialState = {
  productList: dataShoeShop,
  gioHang: [],
};

export const shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      if (index == -1) {
        let newSanPham = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSanPham);
      } else cloneGioHang[index].soLuong++;
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case XOA_SAN_PHAM: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == payload;
      });
      let cloneGioHang = [...state.gioHang];
      cloneGioHang.splice(index, 1);
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case TANG_GIAM_SO_LUONG: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      if (index != -1) {
        cloneGioHang[index].soLuong += payload.giaTri;
      }
      cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
