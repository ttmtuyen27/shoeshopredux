import { createStore } from "redux";
import { rootReducer } from "./reducers/rootReducer";

export const storeShoeShop = createStore(rootReducer);
