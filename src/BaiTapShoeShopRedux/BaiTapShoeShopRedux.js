import React, { Component } from "react";
import Cart from "./Cart";
import ProductList from "./ProductList";

export default class BaiTapShoeShopRedux extends Component {
  render() {
    return (
      <div>
        <ProductList />
        <Cart />
      </div>
    );
  }
}
