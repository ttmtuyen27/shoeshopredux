import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/constants";

class ProductItem extends Component {
  render() {
    let { name, price, image } = this.props.data;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handleAddToCart(this.props.data);
            }}
            className="btn btn-warning"
          >
            Add to cart
          </a>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (sp) => {
      dispatch({
        type: ADD_TO_CART,
        payload: sp,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductItem);
